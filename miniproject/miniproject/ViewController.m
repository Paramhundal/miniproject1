//
//  ViewController.m
//  miniproject
//
//  Created by CLI112 on 10/9/15.
//  Copyright (c) 2015 CLI112. All rights reserved.
//

#import "ViewController.h"
#import "WebViewController.h"


@interface ViewController (){
    UIWebView*viewweb;
    NSInteger* setSelectedButton;

}


@end

@implementation ViewController
//@synthesize viewweb;

NSString *url;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_twitter addTarget:self  action:@selector (touch:)forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_twitter];
    
    [_yahoo addTarget:self  action:@selector (touch:)forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_yahoo];
    
    [_facebook addTarget:self  action:@selector (touch:)forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_facebook];
    
    [_goolleplus addTarget:self  action:@selector (touch:)forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_goolleplus];
    
    [_inst addTarget:self  action:@selector (touch:)forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_inst];
    
    [_linkedin addTarget:self  action:@selector (touch:)forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_linkedin];
    
    [_pintrust addTarget:self  action:@selector (touch:)forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_pintrust];
    
    [_utube addTarget:self  action:@selector (touch:)forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_utube];
    
    [_fliker addTarget:self  action:@selector (touch:)forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_fliker];
    
    [_bing addTarget:self  action:@selector (touch:)forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_bing];
    
    [_opera addTarget:self  action:@selector (touch:)forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_opera];
    
    [_gmail addTarget:self  action:@selector (touch:)forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_gmail];
    
    [_hotmail addTarget:self  action:@selector (touch:)forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_hotmail];
    
    [_hangout addTarget:self  action:@selector (touch:)forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_hangout];
        [super viewDidLoad];
       // NSString *fullURL = @"http://conecode.com";
        //NSURL *url = [NSURL URLWithString:fullURL];
        //NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
       //[viewWeb loadRequest:requestObj];
    }


- (IBAction)touch:(id)sender {

    if ([sender tag] == 1) {
       url = @"https://www.twitter.com/";
       
    }
    else  if ([sender tag] == 2) {
        url = @"https://www.yahoo.com";
       
        // Do something here
    }
    else if ([sender tag]==3){
         url = @"https://www.facebook.com";
        
    }
        
    else if ([sender tag]==4){
        url = @"https://plus.google.com/";
    }
    else if ([sender tag]==5){
        url = @"https://instagram.com/";
    }
    else if ([sender tag]==6){
        url = @"https://linkedin.com/";
    }
    
    else if ([sender tag]==7){
        url = @"https://in.pinterest.com/";
    }
    
    else if ([sender tag]==8){
        url = @"www.youtube.com/‎";
    }
    
    else if ([sender tag]==9){
        url = @"https://www.flickr.com/";
    }
    
    else if ([sender tag]==10){
        url = @"https://www.bing.com/";
    }
    
    else if ([sender tag]==11){
        url = @"www.opera.com/";
    }
    
    else if ([sender tag]==12){
        url = @"https://www.blogger.com/";
    }
    else if ([sender tag]==13){
        url = @"https://mail.google.com/";
    }
    else if ([sender tag]==14){
        url = @"https://login.live.com/";
    }
    else if ([sender tag]==15){
        url = @"https://hangouts.google.com/";
    }
    // Do any additional setup after loading the view, typically from a nib.

    

    
    
//     NSString *fullURL = @"http://conecode.com";
//    NSURL *url = [NSURL URLWithString:fullURL];
//    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
// [viewWeb loadRequest:requestObj];
   [self performSegueWithIdentifier:@"toWebView" sender :nil];
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
   
    
    // This will get called too before the view appears
  
    
    
        if ([[segue identifier] isEqualToString:@"toWebView"]) {
            
            // Get destination view
           WebViewController *vc = [segue destinationViewController];
            
            // Get button tag number (or do whatever you need to do here, based on your object
            vc.fullURL = url;
            
            // Pass the information to your destination view
           //[vc setSelectedButton:tagIndex];
        }
    }

   -(void)setSelectedButton
{
    
}
    
    
    
    
    
    
    
    
    
    
   /* // Make sure your segue name in storyboard is the same as this line
    if ([[segue identifier] isEqualToString:@"toWebView"])
    {
        // Get reference to the destination view controller
        ViewController *vc = [segue WebViewController];
        
        // Pass any objects to the view controller here, like...
        [vc setMyObjectHere:WebViewController];
    }*/


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

//
//  AppDelegate.h
//  miniproject
//
//  Created by CLI112 on 10/9/15.
//  Copyright (c) 2015 CLI112. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


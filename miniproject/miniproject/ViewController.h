//
//  ViewController.h
//  miniproject
//
//  Created by CLI112 on 10/9/15.
//  Copyright (c) 2015 CLI112. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIButton *twitter ;
@property (strong, nonatomic) IBOutlet UIButton *yahoo;
@property (strong, nonatomic) IBOutlet UIButton *facebook;
@property (strong, nonatomic) IBOutlet UIButton *goolleplus;
@property (strong, nonatomic) IBOutlet UIButton *inst;

@property (strong, nonatomic) IBOutlet UIButton *linkedin;
@property (strong, nonatomic) IBOutlet UIButton *pintrust;
@property (strong, nonatomic) IBOutlet UIButton *utube;
@property (strong, nonatomic) IBOutlet UIButton *fliker;
@property (strong, nonatomic) IBOutlet UIButton *bing;
@property (strong, nonatomic) IBOutlet UIButton *opera;
@property (strong, nonatomic) IBOutlet UIButton *gmail;

@property (strong, nonatomic) IBOutlet UIButton *hotmail;
@property (strong, nonatomic) IBOutlet UIButton *hangout;

@end

